
# Reddit Transformer (Tasks 1 & 2)

Get JSON feed from reddit then aggregate or sort and convert it.

##### Aggregate strategies:
  - Sort
  - Aggregate

##### Output formats:
  - CSV
  - SQL

## Installation

Reddit Transformer requires [Node.js](https://nodejs.org/) v4+ to run.

Clone this repository.
```sh
$ git clone git@bitbucket.org:banifaciy777/reddit.git
```
Install the dependencies.
```sh
$ cd reddit
$ npm install --production
```
Start the server.
```sh
$ npm start
```
or
```sh
$ NODE_ENV=production node app
```
or more better way
```sh
$ pm2 startOrReload pm2.json
```
---
#### Extend aggregate strategies or output formats 
Just put Class in `./lib/Transform ` with method `transform()`.

# Tree Generator (Task 3)
Generate `Tree` from array of plain objects.

It don't use recursive calls. It use two iterations. First iteration for build indexes and second iteration for generate tree. 

The complexity of the algorithm: `O(n)`.

#### Start
print result
```sh
$ node tree print
```
benchmark
```sh
$ node tree
```

License
----
MIT
