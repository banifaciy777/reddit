'use strict';

var util = require('util'),
	_ = require('underscore'),
	Abstract = require('./Abstract');

/**
 * Constructor Sort.
 *
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Sort(options, next) {
	options = _.extend({
		sortField: 'id',
		sortDirection: 'asc',
		needFields: ['id', 'title', 'created_utc', 'score']
	}, options);
	
	Abstract.call(this, options, next);
}

util.inherits(Sort, Abstract);

/**
 * Sort feed by custom field and direction.
 *
 * @return {Array[Object]} Sorted feed
 */
Sort.prototype.transform = function() {
	var out, 
		self = this;

	out =  _.chain(this.feed.data.children)
		.map(function (item) {
			item.data.created_utc = self.formatDate(item.data.created_utc);
			return _.pick(item.data, self.options.needFields);
		})
		.sortBy(function(item) { return item[self.options.sortField]; })
		.value();

	if(self.options.sortDirection === 'desc') out.reverse();
	
	return out;
};

/**
 * Convert UNIX timestamp to MySQL date string.
 *
 * @param {String} ts UNIX timestamp
 * @return {String} MySQL date string
 */
Sort.prototype.formatDate = function(ts) {
	var date = new Date(parseInt(ts, 10) * 1000);
	return date.toISOString().slice(0, 19).replace('T', ' ');
};

module.exports = Sort;
