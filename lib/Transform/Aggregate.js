'use strict';

var util = require('util'),
	_ = require('underscore'),
	Abstract = require('./Abstract');

/**
 * Constructor Aggregate.
 *
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Aggregate(options, next) {
	Abstract.call(this, options, next);
}

util.inherits(Aggregate, Abstract);

/**
 * Group feed by domain and sort by count threads.
 *
 * @return {Array[Object]} Aggregated feed
 */
Aggregate.prototype.transform = function() {
	var out = {};

	this.feed.data.children.forEach(function(item) {
		if (out.hasOwnProperty(item.data.domain)) {
			out[item.data.domain].count++;
			out[item.data.domain].score += item.data.score;
		} else {
			out[item.data.domain] = {
				count: 1,
				score: item.data.score
			};
		}
	});

	out =  _.chain(out)
		.map(function(data, domain) {
			return {
				domain: domain,
				count: data.count,
				score: data.score
			};
		})
		.sortBy(function(item) { return -item.count; })
		.value();

	return out;
};


module.exports = Aggregate;
