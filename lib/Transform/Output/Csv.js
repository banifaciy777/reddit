'use strict';

var util = require('util'),
	_ = require('underscore'),
	Abstract = require('./Abstract');

/**
 * Constructor Csv.
 *
 * @param {Object} feed in JSON
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Csv(feed, options, next) {
	options = _.extend({
		separator: ',',
		newLine: '\n'
	}, options);

	Abstract.call(this, feed, options, next);
}

util.inherits(Csv, Abstract);

/**
 * Convert transformed feed JSON to CSV.
 *
 * @return {String} Feed in CSV
 */
Csv.prototype.transform = function() {
	var out,
		self = this;

	out = this.feed.map(function(row) {
		return _.values(row).map(function(field) {
			return self.formatField(field);
		}).join(self.options.separator);
	});

	out = out.join(self.options.newLine);

	return out;
};

/**
 * Sanitize and quote value.
 *
 * @param {String} value
 * @return {String} sanitized and quoted value
 */
Csv.prototype.formatField = function(value) {
	if (typeof value !== 'string') {
		value = value.toString();
	}

	var mustBeQuoted =
		value.indexOf('"') !== -1 ||
		value.indexOf(this.options.separator) !== -1 ||
		value.indexOf(this.options.newLine) !== -1;

	if (mustBeQuoted) {
		value = value.replace(/\"/g, '""');
		value = '"' + value + '"';
	}

	return value;
};


module.exports = Csv;
