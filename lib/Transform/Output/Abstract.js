'use strict';

var Steppy = require('twostep').Steppy;

/**
 * Constructor Abstract.
 *
 * @param {Object} feed in JSON
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Abstract(feed, options, next) {
	this.feed = feed;
	this.options = options;
	
	this.main(next);
}


/**
 * Main Function.
 *
 * @param {Function} next(err, feed)
 */
Abstract.prototype.main = function(next) {
	var self = this;
	Steppy(
		function() {
			this.pass(self.transform());
		},
		function(err, feed) {
			next(null, feed);
		},
		function(err) {
			next(err);
		}
	);
};


module.exports = Abstract;
