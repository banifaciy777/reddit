'use strict';

var util = require('util'),
	_ = require('underscore'),
	Abstract = require('./Abstract');

/**
 * Constructor Sql.
 *
 * @param {Object} feed in JSON
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Sql(feed, options, next) {
	options = _.extend({
		tableName: 'reddit',
		tableFields: null
	}, options);

	Abstract.call(this, feed, options, next);
}

util.inherits(Sql, Abstract);

/**
 * Convert transformed feed JSON to SQL.
 *
 * @return {String} Feed in SQL
 */
Sql.prototype.transform = function() {
	var out,
		self = this;

	out = this.feed.map(function(row) {
		return _.values(row).map(function(field) {
			return self.formatField(field);
		}).join(',');
	});

	out = util.format('INSERT INTO %s (%s) VALUES (%s);',
		self.formatName(this.options.tableName),
		self.generateColumns(),
		out.join('),(')
	);

	return out;
};

/**
 * Generate field names auto or custom.
 *
 * @return {String} Field names. For ex. `id`, `title`
 */
Sql.prototype.generateColumns = function() {

	var fields, originFields;

	originFields = Object.keys(this.feed[0]);

	if(this.options.tableFields) {
		fields = this.options.tableFields
			.split(',')
			.filter(function(item) {
				return item.replace(/[^A-Za-z0-9_]/g, '').trim();
			});

		if(fields.length > originFields.length) {
			fields.length = originFields.length;
		} else if(fields.length < originFields.length) {
			fields = fields.concat(originFields.slice(fields.length));
		}
	} else {
		fields = originFields;
	}

	fields = fields.map(this.formatName).join(', ');

	return fields;
};

/**
 * Sanitize and quote value.
 *
 * @param {String} value
 * @return {String} sanitized and quoted value
 */
Sql.prototype.formatField = function(value) {
	if (typeof value === 'number') {
		return value;
	}

	if (typeof value !== 'string') {
		value = value.toString();
	}

	value = value.replace(/\"/g, '\\"');
	value = '"' + value + '"';

	return value;
};

/**
 * Sanitize and quote name table or field.
 *
 * @param {String} value
 * @return {String} sanitized and quoted table or field name
 */
Sql.prototype.formatName = function(value) {
	if (typeof value !== 'string') {
		value = value.toString();
	}

	value = value.replace(/[^A-Za-z0-9_]/g, '');
	value = '`' + value + '`';

	return value;
};


module.exports = Sql;
