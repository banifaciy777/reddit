'use strict';

exports.sort = require('./Sort');
exports.aggregate = require('./Aggregate');

//Options Schema for validate request params
exports.optionsSchema = {
	properties: {
		action: {
			description: 'Transform action',
			type: 'string',
			'enum': ['sort', 'aggregate'],
			required: true
		},
		url: {
			description: 'Reddit URL for transform',
			type: 'string',
			pattern: '^https\:\/\/www\.reddit\.com\/r\/[A-Za-z0-9_-]+\/\.json$',
			required: true
		},
		outputFormat: {
			description: 'Output format (CSV or SQL)',
			type: 'string',
			'enum': ['csv', 'sql'],
			required: true
		},
		sortField: {
			description: 'Field name for sort result',
			type: 'string',
			'enum': ['id', 'title', 'created_utc', 'score']
		},
		sortDirection: {
			description: 'Direction for sort result',
			type: 'string',
			'enum': ['asc', 'desc']
		},
		separator: {
			description: 'CSV Separator for output formatter',
			type: 'string',
			minLength: 1
		},
		tableName: {
			description: 'SQL table name for output formatter',
			type: 'string',
			pattern: '^[a-zA-Z0-9-_]+$',
			minLength: 1
		},
		tableFields: {
			description: 'SQL table fields for output formatter',
			type: 'string',
			pattern: '^[a-zA-Z0-9-_,\\s]*$'
		}
	}
};
