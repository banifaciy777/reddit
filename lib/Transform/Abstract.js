'use strict';

var https = require('https'),
	_ = require('underscore'),
	Steppy = require('twostep').Steppy,
	Output = require('./Output');

/**
 * Constructor Abstract.
 *
 * @param {Object} options
 * @param {Function} next(err, feed)
 */
function Abstract(options, next) {
	this.options = _.extend({
		url: 'https://www.reddit.com/r/javascript/.json',
		outputFormat: 'csv'
	}, options);
	
	this.main(next);
}


/**
 * Main Function.
 *
 * @param {Function} next(err, feed)
 */
Abstract.prototype.main = function(next) {
	var self = this;
	Steppy(
		function() {
			self.downloadFeed(this.slot());
		},
		function(err) {
			self.convertFeed(this.slot());
		},
		function(err) {
			this.pass(self.transform());
		},
		function(err, feed) {
			new Output[self.options.outputFormat](feed, self.options, this.slot());
		},
		function(err, feed) {
			next(null, feed);
		},
		function(err) {
			next(err);
		}
	);
};

/**
 * Download feed from server.
 *
 * @param {Function} next(err, feed)
 */
Abstract.prototype.downloadFeed = function(next) {
	var self = this;
	https.get(this.options.url, function(res) {
		if (res.statusCode !== 200) {
			next('Error status code: ' + res.statusCode);
			return;
		}

		var data = '';
		// consume response body
		res.resume();
		res.on('data', function(chunk) {
			data += chunk;
		}).on('end', function() {
			self.feed = data;
			next(null, data);
		});
	}).on('error', function(err) {
		next(err);
	});
};

/**
 * Convert feed from string to JSON Object.
 *
 * @param {Function} next(err)
 */
Abstract.prototype.convertFeed = function(next) {
	try {
		this.feed = JSON.parse(this.feed);
	} catch(err) {
		next(err);
		return;
	}

	next(null);
};

module.exports = Abstract;
