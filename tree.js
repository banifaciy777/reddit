'use strict';

var util = require('util'),
	treeData = require('./treeData'),
	map = {},
	k,
	index,
	branch;

console.time('Generate tree');

for (k = 0; k < treeData.length; k++) {
	branch = treeData[k];

	if(!map[branch.parentId]) map[branch.parentId] = {};
	if(map[branch.parentId].children) {
		index = map[branch.parentId].children.length;
		map[branch.parentId].children[index] = branch;
		map[branch.id] = map[branch.parentId].children[index];
	} else {
		map[branch.parentId].children = [branch];
		map[branch.id] = map[branch.parentId].children[0];
	}
}

console.timeEnd('Generate tree');
console.log('Tree length = %d', treeData.length);

//Print result if last arg is 'print'
if(process.argv.slice(-1)[0] === 'print')
	console.log('Tree:\n', util.inspect(map[0].children, {depth: 10}));