'use strict';

var express = require('express'),
	routes = require('./routes'),
	app = express();

//Init template engine
app.set('view engine', 'jade');

//Init app routes
app.get('/', routes.form);
app.get('/result', routes.result);

//Error handler
app.use(routes.errorHandler);

//Not found handler
app.use(routes.notFoundHandler);

//Log uncaught exception and exit
process.on('uncaughtException', function(err) {
	console.error('Uncaught Exception:\n', err);
	process.exit(1);
});

//Start server
app.listen(process.env.PORT || 3000, function() {
	console.log('App listening on port %s!', process.env.PORT || 3000);
});
