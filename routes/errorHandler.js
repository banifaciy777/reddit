'use strict';

module.exports = function (err, req, res, next) {
	//Log error
	console.error(err);
	//Send Internal Server Error
	res.sendStatus(500);
};
