'use strict';

module.exports = function (req, res) {
	res.locals.query = req.query;
	res.render('form', res.locals);
};