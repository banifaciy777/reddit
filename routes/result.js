'use strict';

var conform = require('conform'),
	Transform = require('../lib/Transform');

module.exports = function(req, res) {

	var optionsConform = conform.validate(req.query, Transform.optionsSchema);

	//If options is invalid then render errors
	if(!optionsConform.valid) {
		render(optionsConform.errors);
		return;
	}

	var action = req.query.action || 'sort';

	//Init transform class by action
	new Transform[action] (req.query, render);

	//Function for render result or error
	function render(err, feed) {
		if (err) {
			console.error(err);
			res.locals.query = req.query;
			res.locals.error = err;
			res.render('form', res.locals);
			return;
		}

		res.type('text/plain').send(feed);
	}

};